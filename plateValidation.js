

function isValidPlate(plate) { // eslint-disable-line no-unused-vars
  var re = /(\d\d\d\d[BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ])/i;
  var res = plate.match(re) != null;
  gtag('event', 'click', { // eslint-disable-line
    'event_category': 'calculate',
    'event_label': res 
  });
  
  return res;
}

