

test("Basic correct cases", function (assert) {
    assert.equal(isValidPlate("1234BCD"), true, "DEFAULT PLATE UPPER CASE");
    assert.equal(isValidPlate("5778fgh"), true, "DEFAULT PLATE LOWER CASE");
    assert.equal(isValidPlate("9012pnL"), true, "DEFAULT PLATE MIXES CASE");
});

test("Human error", function (assert) {
    assert.equal(isValidPlate("5555SSS"), true, "Correct 5~S");
    assert.equal(isValidPlate("55555SS"), false, "1 more 5");
    assert.equal(isValidPlate("555SSSS"), false, "1 more S");
    assert.equal(isValidPlate("1234 BCD"), false, "Spaced");
});

test("Invalid characters", function (assert) {
    assert.equal(isValidPlate("0051abc"), false, "VOWEL");
    assert.equal(isValidPlate("6385QRT"), false, "Q LETTER");
    assert.equal(isValidPlate("2352ÑPJ"), false, "TWO PLATES");
    assert.equal(isValidPlate("23º5WPJ"), false, "SPECIAL CHARACTERS 1 (º)");
    assert.equal(isValidPlate("5625#PJ"), false, "SPECIAL CHARACTERS 2 (#)");
    assert.equal(isValidPlate("2354WF+"), false, "SPECIAL CHARACTERS 3 (+)");
});

test("Different order", function (assert) {
    assert.equal(isValidPlate("sjd2105"), false, "REVERSED");
    assert.equal(isValidPlate("V2525FS"), false, "MIXED 1");
    assert.equal(isValidPlate("442GF5F"), false, "MIXED 2");
    assert.equal(isValidPlate("D245DF4"), false, "MIXED 3");
});

test("Incorrect lengths", function (assert) {
	// Comment failing plates
    //assert.equal(isValidPlate("11234BCDD"), false, "MORE NUMBERS AND LETTERS");
    //assert.equal(isValidPlate("1234BCDD"),  false, "MORE LETTERS");
    //assert.equal(isValidPlate("11234BCD"),  false, "MORE NUMBERS");
    //assert.equal(isValidPlate("646184684111234BCDDVLFDHJVJAÑQ"), false, "A LOT MORE NUMBERS AND LETTERS");
    //assert.equal(isValidPlate("1234BCD5678FGH"), false, "TWO PLATES");
    assert.equal(isValidPlate("234BC"),     false, "LESS NUMBERS AND LETTERS");
    assert.equal(isValidPlate("234BCD"),    false, "LESS NUMBERS");
    assert.equal(isValidPlate("1234BC"),    false, "LESS LETTERS");
});